# Prova TILIX
  
Para rodar o projeto certifique-se de ter instalado em sua máquina **docker** e **docker-compose**.
***
### Acesso aplicação
Para rodar basta executar `./build.sh` após rodar esse script a aplicação responderá em `localhost:80`. 
### Acesso banco de dados
Para acessar o bando de dados acesse `http://localhost:5050` email: `barbosa.olivera1@gmail.com`  e senha `tilixpass` para acessar o schema crie um server com a seguintes configurações host: `postgres_container`, user: `tilix` password: `tilixpass`.