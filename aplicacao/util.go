package aplicacao

import (
	"log"
	"provatilix/model"
)

func TrataErroConexao(erroApp *model.Erro, err error) (*model.Erro) {
	if erroApp != nil {
		return erroApp
	}
	log.Fatal("\tErro transação banco de dados \t " + err.Error())
	return &model.Erro{"SERVICO_UTIL_10", "Erro transação banco de dados", err}
}

