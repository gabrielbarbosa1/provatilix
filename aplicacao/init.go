package aplicacao

import (
	"database/sql"
	"log"
	"provatilix/infraestrutura/repositorio/postgres"
	repFatura "provatilix/infraestrutura/repositorio/postgres/fatura"
)

func Init() (*error) {
	if db, erro := postgres.FindDb(); erro  == nil {
		if errTx := postgres.Transact(db, func(tx *sql.Tx) error {
			repFatura.CreateTable(tx)
			return nil
		}); errTx != nil {
			log.Fatal(errTx)
			return &errTx
		}
	} else  {
		log.Fatal(erro)
		return &erro.Err
	}
	return nil
}
