package aplicacao

import (
	"database/sql"
	"provatilix/infraestrutura/repositorio/postgres"
	"provatilix/model"
	repFatura "provatilix/infraestrutura/repositorio/postgres/fatura"
)

func CadastrarFatura(fatura model.Fatura) (erro *model.Erro, f model.Fatura) {
	if erro, _ = model.NewFatura(&fatura); erro != nil {
		return erro,  model.Fatura{}
	}
	if db, erro := postgres.FindDb(); erro  == nil {
		if errTx := postgres.Transact(db, func(tx *sql.Tx) error {
			fatura.IdFatura, erro = repFatura.Save(tx, fatura)
			return nil
		}); errTx != nil {
			return TrataErroConexao(erro, errTx),  model.Fatura{}
		}
	}
	return nil, fatura
}

func AtualizarFatura(fatura model.Fatura) (erro *model.Erro, f model.Fatura) {
	if erro, _ = model.NewFatura(&fatura); erro != nil {
		return erro, model.Fatura{}
	}
	if db, erro := postgres.FindDb(); erro == nil {
		if errTx := postgres.Transact(db, func(tx *sql.Tx) error {
			var existe bool = false
			if existe, erro = repFatura.ExistsById(tx, fatura.IdFatura); existe == false {
				erro = &model.Erro{"FATURA_SERVICE_10", "Fatura não encontrada na base de dados!", nil}
				return nil
			}
			erro = repFatura.Update(tx, fatura)
			return nil
		}); errTx != nil {
			return TrataErroConexao(erro, errTx), model.Fatura{}
		}
	}

	return nil, fatura
}

func ExcluirFatura(id int) (erro *model.Erro) {
	if id <= 0 {
		return &model.Erro{"FATURA_SERVICE_20", "Identificador inválido!", nil}
	}
	if db, erro := postgres.FindDb(); erro == nil {
		if errTx := postgres.Transact(db, func(tx *sql.Tx) error {
			erro = repFatura.DeleteById(tx, id)
			return nil
		}); errTx != nil {
			return TrataErroConexao(erro, errTx)
		}
	}
	return nil
}

func ConsultarFatura() (erro *model.Erro, faturas []model.Fatura) {
	if db, erro := postgres.FindDb(); erro == nil {
		if errTx := postgres.Transact(db, func(tx *sql.Tx) error {
			faturas, erro = repFatura.FindAll(tx)
			return nil
		}); errTx != nil {
			return TrataErroConexao(erro, errTx), faturas
		}
	}
	return nil, faturas
}

