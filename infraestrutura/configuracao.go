package infraestrutura

import "time"

const (
	DATA_FORMAT = "02/01/2006"
	TIMESTAMP_FORMAT = time.RFC3339
)

var localeApp *time.Location

func TimeZonePadrao() (*time.Location) {
	if localeApp != nil {
		localeApp,_ = time.LoadLocation("America/Sao_Paulo")
	}
	return localeApp
}