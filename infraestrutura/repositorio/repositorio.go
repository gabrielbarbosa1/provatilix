package repositorio

import (
	"database/sql"
	"provatilix/model"
)

type Repository interface {
	Count(tx *sql.Tx) (int, *model.Erro)

	Delete (tx *sql.Tx, entidade interface {}) (*model.Erro)

	DeleteAll (tx *sql.Tx) (*model.Erro)

	DeleteSlice (tx *sql.Tx, entidades []interface{}) (*model.Erro)

	DeleteById(tx *sql.Tx, id int) (*model.Erro)

	ExistsById(tx *sql.Tx, id int) (bool, *model.Erro)

	FindAll(tx *sql.Tx) (entidades []interface{}, erro *model.Erro)

	FindAllById(tx *sql.Tx, ids []int) (entidades []interface{}, erro *model.Erro)

	FindById(tx *sql.Tx, id int) (entidades interface{}, erro *model.Erro)

	Save(tx *sql.Tx, entidade interface{}) (int, *model.Erro)

	SaveAll(tx *sql.Tx, entidades []interface{}) (*model.Erro)

	Update(tx *sql.Tx, entidade interface{}) (*model.Erro)
	
	BuscaPaginada(tx *sql.Tx, paginaSolicitada model.Pagina, entidade interface{}, campos[]string) (pagina model.Pagina, erro *model.Erro)
}
