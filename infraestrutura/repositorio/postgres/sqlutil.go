package postgres

import (
	"fmt"
	"provatilix/infraestrutura"
	"provatilix/model"
	"strconv"
	"time"
)

//Cria (1,2,3,4,5) de acordo
//com o array dado.
func InCreate(ids []int) (string) {
	in := "("
	for i := 0; i < len(ids); i++ {
		in += strconv.Itoa(ids[i])
		in += ","
	}
	in = in[0:len(in)-1]
	in += ")"
	return in
}

func InsertInto(tableName string, idName string, columns []string, types []string, values []interface{}) (string) {
	sql := "INSERT INTO " + tableName
	sql += "("
	for i := 0; i < len(columns); i++ {
		sql += columns[i]
		sql += ","
	}
	sql = sql[0:len(sql)-1]
	sql += ") VALUES ("

	for i := 0; i < len(values); i++ {
		if (types[i] == "string") {
			sql +=  "'" + values[i].(string) + "'"
			sql += ","
		}
		if (types[i] == "int") {
			sql += strconv.Itoa(values[i].(int))
			sql += ","
			continue
		}
		if (types[i] == "float") {
			sql += fmt.Sprintf("%.2f", values[i].(float32))
			sql += ","
			continue
		}
		if (types[i] == "date") {
			sql += "'" + AjustarDataPostgres(values[i].(string)) + "'"
			sql += ","
			continue
		}
		if (types[i] == "bool") {
			if values[i].(bool) {
				sql += "TRUE,"
				continue
			}
			sql += "FALSE,"
		}
	}
	sql = sql[0:len(sql)-1]
	sql += ") RETURNING  " + idName + ";"
	return sql
}

func AjustarDataPostgres(dataString string) (string) {
	if data, err := time.Parse(infraestrutura.DATA_FORMAT, dataString); err != nil {
		return dataString;
	} else {
		ano, mes, day := data.Date()
		return strconv.Itoa(ano) + "-" + strconv.Itoa(int(mes)) + "-" + strconv.Itoa(day)
	}
}

func WhereWithAnds(filtros []string) (string) {
	sql := " "
	for i := 0; i < len(filtros); i++ {
		sql += filtros[i] + " AND"
	}
	sql = sql[0:len(sql)-len(" AND")]
	return sql
}

func WhereWithOrs(filtros []string) (string) {
	sql := ""
	for i := 0; i < len(filtros); i++ {
		sql += filtros[i] + " OR"
	}
	sql = sql[0:len(sql)-len(" OR")]
	return sql
}

func BoolValue(logico bool) (string) {
	if logico {
		return "TRUE"
	}
	return "FALSE"
}

func GerarSqlOfsset(pagina model.Pagina) string {
	return "OFFSET " + strconv.Itoa((pagina.NumPagina-1)*pagina.QtdPorPagina) + " LIMIT " + strconv.Itoa(pagina.QtdPorPagina)
}