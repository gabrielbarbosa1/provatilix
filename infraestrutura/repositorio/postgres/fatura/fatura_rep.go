package fatura

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"provatilix/infraestrutura/repositorio/postgres"
	"provatilix/model"
	"strconv"
)

var ctx = context.Background()

func CreateTable(tx *sql.Tx) {
	sql := " CREATE SEQUENCE IF NOT EXISTS fatura_sequence INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;"
	sql += " CREATE TABLE IF NOT EXISTS FATURA ("
	sql += " IdFatura BIGINT DEFAULT nextval('fatura_sequence') PRIMARY KEY, "
	sql += " IdUsuario BIGINT, "
	sql += " Nome_Empresa VARCHAR (100), "
	sql += " Valor NUMERIC (10,2), "
	sql += " Data_Vencimento DATE, "
	sql += " Pago BOOLEAN"
	sql += ");"
	tx.Exec(sql)
}

func Count(tx * sql.Tx)  (int, *model.Erro) {
	var qtd int = 0
	stmt, err := tx.PrepareContext(ctx, "SELECT COUNT(*) AS qtd FROM FATURA")
	if err != nil {
		return 0, &model.Erro{"FATURA_REP10", "Erro banco Count", err}
	}
	defer stmt.Close()
	err = stmt.QueryRowContext(ctx).Scan(&qtd)
	switch {
	case err == sql.ErrNoRows:
		return 0, nil
	case err != nil:
		log.Println(err)
		return 0, &model.Erro{"ATRIBUTO_IMOVEL_REP10", "Erro banco Count", err}
	default:
		return qtd, nil
	}
}

func Delete (tx *sql.Tx, entidade model.Fatura) (*model.Erro) {
	id := entidade.IdFatura
	stmt, err := tx.PrepareContext(ctx, "DELETE FROM FATURA WHERE IdFatura = $1")
	if err != nil {
		return &model.Erro{"FATURA_REP15", "Erro banco Delete", err}
	}
	defer stmt.Close()
	if _, err := stmt.ExecContext(ctx, id); err != nil {
		return &model.Erro{"FATURA_REP15", "Erro banco Delete", err}
	}
	return nil
}

func DeleteAll (tx *sql.Tx) (*model.Erro) {
	stmt, err := tx.PrepareContext(ctx, "DELETE FROM FATURA")
	if err != nil {
		log.Println(err)
		return &model.Erro{"FATURA_REP20", "Erro banco DeleteAll", err}
	}
	defer stmt.Close()
	if _, err := stmt.ExecContext(ctx); err != nil {
		log.Println(err)
		return &model.Erro{"FATURA_REP20", "Erro banco DeleteAll", err}
	}
	return nil
}

func DeleteSlice (tx *sql.Tx, entidades []model.Fatura) (*model.Erro) {
	in := ""
	var inId []int
	for i := 0; i < len(entidades); i++ {
		inId = append(inId, entidades[i].IdFatura)
	}
	in = postgres.InCreate(inId)
	stmt, err := tx.PrepareContext(ctx, "DELETE FROM FATURA WHERE IdFatura IN " + in)
	if err != nil {
		log.Println(err)
		return &model.Erro{"FATURA_REP25", "Erro banco DeleteSlice", err}
	}
	defer stmt.Close()
	if _, err := stmt.ExecContext(ctx); err != nil {
		log.Println(err)
		return &model.Erro{"FATURA_REP25", "Erro banco DeleteSlice", err}
	}
	return nil
}

func DeleteById(tx *sql.Tx, id int) (*model.Erro) {
	stmt, err := tx.PrepareContext(ctx, "DELETE FROM FATURA WHERE IdFatura = $1")
	if err != nil {
		log.Println(err)
		return &model.Erro{"FATURA_REP30", "Erro banco DeleteById", err}
	}
	defer stmt.Close()
	if _, err := stmt.ExecContext(ctx, id); err != nil {
		log.Println(err)
		return &model.Erro{"FATURA_REP30", "Erro banco DeleteById", err}
	}
	return nil
}

func ExistsById(tx *sql.Tx, id int) (bool, *model.Erro) {
	var qtd int
	stmt, err := tx.PrepareContext(ctx, "SELECT COUNT(*) AS qtd FROM FATURA WHERE IdFatura = $1")
	if err != nil {
		log.Println(err)
		return false, &model.Erro{"FATURA_REP35", "Erro banco ExistsById", err}
	}
	defer stmt.Close()
	err = stmt.QueryRowContext(ctx, id).Scan(&qtd)
	switch {
	case err == sql.ErrNoRows:
		return false, nil
	case err != nil:
		log.Println(err)
		return false, &model.Erro{"FATURA_REP35", "Erro banco ExistsById", err}
	default:
		if qtd == 0 {
			return false, nil
		} else {
			return true, nil
		}
	}
}

func FindAll(tx *sql.Tx) (entidades []model.Fatura, erro *model.Erro) {
	stmt, err := tx.PrepareContext(ctx,"SELECT IdFatura, IdUsuario, Nome_Empresa, Valor, Data_Vencimento, Pago FROM FATURA")
	if err != nil {
		log.Println(err)
		return nil, &model.Erro{"FATURA_REP40", "Erro banco FindAll", err}
	}
	defer stmt.Close()
	rows, err:= stmt.QueryContext(ctx)
	if err != nil {
		log.Println(err)
		return nil, &model.Erro{"FATURA_REP40", "Erro banco FindAll", err}
	}
	entidades, err = parserFatura(rows)
	if err != nil {
		log.Println(err)
		return nil, &model.Erro{"FATURA_REP40", "Erro banco FindAll", err}
	}
	return entidades, nil
}

func FindAllById(tx *sql.Tx, ids []int) (entidades []model.Fatura, erro *model.Erro) {
	stmt, err := tx.PrepareContext(ctx, "SELECT IdFatura, IdUsuario, Nome_Empresa, Valor, Data_Vencimento, Pago FROM FATURA FROM FATURA WHERE IdFatura IN " + postgres.InCreate(ids))
	if err != nil {
		log.Println(err)
		return nil, &model.Erro{ "FATURA_REP45", "Erro banco FindAllById", err}
	}
	defer stmt.Close()
	rows, err := stmt.QueryContext(ctx)
	if err != nil {
		log.Println(err)
		return nil, &model.Erro{ "FATURA_REP45", "Erro banco FindAllById", err}
	}
	entidades, err = parserFatura(rows)
	if err != nil {
		log.Println(err)
		return nil, &model.Erro{ "FATURA_REP45", "Erro banco FindAllById", err}
	}
	return entidades, nil
}


func FindById(tx *sql.Tx, id int) (entidade model.Fatura, erro *model.Erro) {
	stmt, err := tx.PrepareContext(ctx, "SELECT IdFatura, IdUsuario, Nome_Empresa, Valor, Data_Vencimento, Pago FROM FATURA WHERE IdFatura = $1")
	if err != nil {
		log.Println(err)
		return model.Fatura{}, &model.Erro{ "FATURA_REP50", "Erro banco FindById", err}
	}
	defer stmt.Close()
	rows, err := stmt.QueryContext(ctx, id)
	if err != nil {
		log.Println(err)
		return model.Fatura{}, &model.Erro{ "FATURA_REP50", "Erro banco FindById", err}
	}
	entidades, err := parserFatura(rows)
	if err != nil {
		log.Println(err)
		return model.Fatura{}, &model.Erro{ "FATURA_REP50", "Erro banco FindById", err}
	}
	if (len(entidades) > 0) {
		return entidades[0], nil
	} else {
		return model.Fatura{}, nil
	}
}


func Save(tx *sql.Tx, entidade model.Fatura) (int, *model.Erro) {
	sqlInsert := insertIntoFatura(entidade)
	stmt, err := tx.PrepareContext(ctx, sqlInsert)
	if err != nil {
		log.Println(err)
		return  0, &model.Erro{ "FATURA_REP60", "Erro banco SaveAll", err}
	}
	defer stmt.Close()

	rows, err := stmt.QueryContext(ctx)
	if err != nil {
		log.Println(err)
		return  0, &model.Erro{ "FATURA_REP60", "Erro banco SaveAll", err}
	}
	defer rows.Close()

	id := 0
	for rows.Next() {
		if err = rows.Scan(&id); err != nil {
			log.Println(err)
			return  0, &model.Erro{ "FATURA_REP60", "Erro banco SaveAll", err}
		}
	}
	return id, nil
}

func SaveAll(tx *sql.Tx, entidades []model.Fatura) (*model.Erro) {
	sqlInsert := ""

	for i := 0; i < len(entidades); i++ {
		sqlInsert += insertIntoFatura(entidades[i])
	}

	if _,errTx := tx.Exec(sqlInsert); errTx != nil {
		return &model.Erro{"FATURA_REP60", "Erro banco SaveAll", errTx}
	}
	return nil
}

func Update(tx *sql.Tx, entidade model.Fatura) (*model.Erro) {
	if _, errTx := tx.Exec(updateIntoFatura(entidade)); errTx != nil {
		return &model.Erro{"FATURA_REP65", "Erro banco Update", errTx}
	}
	return nil
}


func BuscaPaginada(tx *sql.Tx, paginaSolicitada model.Pagina, entidade model.Fatura, campos[]string) (pagina model.Pagina, erro *model.Erro) {
	var totalRegistro int = 0
	pagina.NumPagina = paginaSolicitada.NumPagina
	pagina.QtdPorPagina = paginaSolicitada.QtdPorPagina

	sqlCount := " SELECT COUNT(*) FROM FATURA " + whereLikeFatura(entidade, campos)
	err := tx.QueryRow(sqlCount).Scan(&totalRegistro)
	switch {
	case err == sql.ErrNoRows:
		return paginaSolicitada, &model.Erro{"FATURA_REP120", "Erro banco busca paginada", err}
	case err != nil:
		log.Println(err)
		return paginaSolicitada, &model.Erro{"FATURA_REP120", "Erro banco busca paginada", err}
	default:
		pagina.TotalRegistro = totalRegistro
		pagina.TotalPagina = model.CalcQtdPaginas(pagina.TotalRegistro, pagina.QtdPorPagina)
	}

	sqlQuery := "SELECT IdFatura, IdUsuario, Nome_Empresa, Valor, Data_Vencimento, Pago FROM FATURA " + whereLikeFatura(entidade, campos) + " ORDER BY IdFatura"


	sqlQuery += postgres.GerarSqlOfsset(pagina) + ";"

	var conteudo []model.Fatura

	rows, errTx := tx.Query(sqlQuery)
	if errTx != nil {
		return paginaSolicitada, &model.Erro{"FATURA_REP50", "Erro banco FindById", errTx}
	}
	conteudo, err = parserFatura(rows)
	if err != nil {
		return paginaSolicitada, erro
	}
	pagina.Conteudo = conteudo
	return pagina, nil
}


func parserFatura(rows *sql.Rows) ([]model.Fatura, error) {
	defer rows.Close()
	var results []model.Fatura
	for rows.Next() {
		fat := model.Fatura{}
		if err := rows.Scan(&fat.IdFatura, &fat.IdUsuario, &fat.NomeEmpresa, &fat.Valor, &fat.DataVencimento, &fat.Pago); err != nil {
			return nil, err
		}
		results = append(results, fat)
	}
	if rows.Err() != nil {
		return nil, rows.Err()
	}
	return results, nil
}

func whereLikeFatura(fatura model.Fatura, campos []string) (sql string) {
	if (len(campos) > 0) {
		var filters = []string{}
		for i := 0; i < len(campos); i++ {
			switch campos[i] {
				case "IdUsuario":
					filters = append(filters, "IdUsuario = " + strconv.Itoa(fatura.IdUsuario))
				case "Nome_Empresa":
					filters = append(filters, "UPPER(Nome_Empresa) LIKE '%" + fatura.NomeEmpresa + "%'")
				case "Valor":
					filters = append(filters, "Valor = " + fmt.Sprint("%.2f", fatura.Valor))
				case "Data_Vencimento":
					filters = append(filters, "Data_Vencimento = " + postgres.AjustarDataPostgres(fatura.DataVencimento))
				case "Pago":
					filters = append(filters, "Pago = " + postgres.BoolValue(fatura.Pago) )
			}
		}
		return " WHERE " + postgres.WhereWithAnds(filters)
	}
	return ""
}

func insertIntoFatura(fatura model.Fatura) (string) {
	var values []interface{}
	values = append(values, fatura.IdUsuario)
	values = append(values, fatura.NomeEmpresa)
	values = append(values, fatura.Valor)
	values = append(values, fatura.DataVencimento)
	values = append(values, fatura.Pago)
	return postgres.InsertInto("FATURA", "IdFatura",
			[]string{"IdUsuario", "Nome_Empresa", "Valor", "Data_Vencimento", "Pago"},
			[]string{"int", "string", "float", "date", "bool"},
			values)
}

func updateIntoFatura(fatura model.Fatura) (string) {
	return "UPDATE FATURA SET " +
			" IdUsuario=" + strconv.Itoa(fatura.IdUsuario) + "," +
			" Nome_Empresa='" + fatura.NomeEmpresa + "'," +
			" Valor=" + fmt.Sprintf("%.2f", fatura.Valor) + "," +
		 	" Data_Vencimento='" + postgres.AjustarDataPostgres(fatura.DataVencimento) + "'," +
			" Pago=" + postgres.BoolValue(fatura.Pago) +
			" WHERE IdFatura =" + strconv.Itoa(fatura.IdFatura)
}
