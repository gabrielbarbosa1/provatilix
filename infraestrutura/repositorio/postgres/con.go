package postgres

import (
	"database/sql"
	"log"
	"provatilix/model"
)
var connStr string = "user=tilix dbname=tilix password='tilixpass' sslmode=disable"
var db *sql.DB
var dbTest *sql.DB

func FindDb() (*sql.DB, *model.Erro) {
	if db == nil {
		var err error
		db, err = sql.Open("postgres", connStr)
		if err != nil {
			log.Fatal(err)
			return nil, &model.Erro{"CON_10", "Erro banco FindDb", err}
		}
	}
	return db, nil
}

//
// Permite o controle e execução de vária transações
// de maneira que as transações fiquem aninhadas umas
// as outras e qualquer problema a função identifica
// e realiza o roolback da transação.
//func (s Service) DoSomething() error {
//    return Transact(s.db, func (tx *sql.Tx) error {
//        if _, err := tx.Exec(...); err != nil {
//            return err
//        }
//        if _, err := tx.Exec(...); err != nil {
//            return err
//        }
//    })
//}
//
//
//
func Transact(db *sql.DB, txFunc func(*sql.Tx) error) (err error) {
	tx, err := db.Begin()
	if err != nil {
		return
	}
	defer func() {
		if p := recover(); p != nil {
			tx.Rollback()
			panic(p) // re-throw panic after Rollback
		} else if err != nil {
			tx.Rollback()
		} else {
			err = tx.Commit()
		}
	}()
	err = txFunc(tx)
	return err
}

