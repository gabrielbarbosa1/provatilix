import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';
import {routedComponents, ZtilixpagesRoutingModule} from "./ztilixpages-routing.module";
import { NgxCurrencyModule } from "ngx-currency";

export const customCurrencyMaskConfig = {
  align: "right",
  allowNegative: true,
  allowZero: true,
  decimal: ",",
  precision: 2,
  prefix: "R$ ",
  suffix: "",
  thousands: ".",
  nullable: true
};

@NgModule({
  imports: [
    ThemeModule,
    ZtilixpagesRoutingModule,
    NgxCurrencyModule.forRoot(customCurrencyMaskConfig)
  ],
  declarations: [
    ...routedComponents,
  ],
})
export class ZtilixpagesModule {}
