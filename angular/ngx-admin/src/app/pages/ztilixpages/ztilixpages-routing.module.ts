import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {NgxMaskModule} from 'ngx-mask'


import { FaturaComponent } from './fatura/fatura.component';
import {HttpClientModule} from "@angular/common/http";

const routes: Routes = [{
  path: '',
  component: FaturaComponent,
}];

@NgModule({
  imports: [RouterModule.forChild(routes),
            NgxMaskModule.forRoot(),
            HttpClientModule],
  exports: [RouterModule],
})
export class ZtilixpagesRoutingModule {
}

export const routedComponents = [FaturaComponent]
