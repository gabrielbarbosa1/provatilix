import {NbMenuService, NbThemeService, NbCheckboxComponent} from '@nebular/theme';
import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Fatura} from "./fatura";
import {SolarData} from "../../../@core/data/solar";
import {takeWhile} from "rxjs/operators";
import {HttpClient} from "@angular/common/http";
import {NotificationsService} from "angular2-notifications";
import {Erro} from "./erro";
import { Observable } from 'rxjs';

interface CardSettings {
  title: string;
  iconClass: string;
  type: string;
}

@Component({
  selector: 'fatura-component',
  styleUrls: ['./fatura.component.scss'],
  templateUrl: './fatura.component.html',
})
export class FaturaComponent implements OnDestroy, OnInit {

  private alive = true;

  solarValue: number;
  lightCard: CardSettings = {
    title: 'Light',
    iconClass: 'nb-lightbulb',
    type: 'primary',
  };
  rollerShadesCard: CardSettings = {
    title: 'Roller Shades',
    iconClass: 'nb-roller-shades',
    type: 'success',
  };
  wirelessAudioCard: CardSettings = {
    title: 'Wireless Audio',
    iconClass: 'nb-audio',
    type: 'info',
  };
  coffeeMakerCard: CardSettings = {
    title: 'Coffee Maker',
    iconClass: 'nb-coffee-maker',
    type: 'warning',
  };

  statusCards: string;

  commonStatusCardsSet: CardSettings[] = [
    this.lightCard,
    this.rollerShadesCard,
    this.wirelessAudioCard,
    this.coffeeMakerCard,
  ];

  statusCardsByThemes: {
    default: CardSettings[];
    cosmic: CardSettings[];
    corporate: CardSettings[];
  } = {
    default: this.commonStatusCardsSet,
    cosmic: this.commonStatusCardsSet,
    corporate: [
      {
        ...this.lightCard,
        type: 'warning',
      },
      {
        ...this.rollerShadesCard,
        type: 'primary',
      },
      {
        ...this.wirelessAudioCard,
        type: 'danger',
      },
      {
        ...this.coffeeMakerCard,
        type: 'secondary',
      },
    ],
  };

  constructor(private themeService: NbThemeService,
              private solarService: SolarData,
              private menuService: NbMenuService,
              private notificationService: NotificationsService,
              private httpClient: HttpClient) {
    this.themeService.getJsTheme()
        .pipe(takeWhile(() => this.alive))
        .subscribe(theme => {
          this.statusCards = this.statusCardsByThemes[theme.name];
        });

    this.solarService.getSolarData()
        .pipe(takeWhile(() => this.alive))
        .subscribe((data) => {
          this.solarValue = data;
        });
  }
  public textoPesquisa: string;
  public faturaSelecionada: Fatura = new Fatura();
  public faturas: Fatura[] = new Array<Fatura>();
  public filtro: string;
  public url: string =  window.location.protocol + "//" + window.location.hostname + "/faturas";

  @ViewChild("pagoChield") 
  public pagoChield : NbCheckboxComponent;

  ngOnDestroy() {
    this.alive = false;
  }

  ngOnInit(): void {
        this.findAll();
  }

  onChangeLimpar() {
    this.faturaSelecionada = new Fatura();
    this.pagoChield.writeValue(false);
  }

  onChangeDeletar() {
      this.httpClient.delete<Fatura>(this.url + "/" + this.faturaSelecionada.idFatura)
          .subscribe((fat) => {
              this.onSucessMensage("Fatura", "Fatura apagada com sucesso!")
              this.findAll();
          });
  }

  onChangeCadastrar() {
    this.faturaSelecionada.pago = this.pagoChield.value;
    if (this.faturaSelecionada.idFatura != null) {
        this.httpClient.put<Fatura>(this.url, this.faturaSelecionada)
            .subscribe((fat) => {
                this.onSucessMensage("Fatura", "Fatura atualizada com sucesso!")
                this.findAll();
                this.onChangeLimpar();
            }, error1 => this.onError(error1.error));
    } else {
        this.httpClient.post<Fatura>(this.url, this.faturaSelecionada)
            .subscribe((fat) => {
                this.onSucessMensage("Fatura", "Fatura cadastrada com sucesso!")
                this.findAll();
                this.onChangeLimpar();
            }, (error1 => this.onError(error1.error)));
    }
  }

  onChangeSelecionarFatura(fatura: Fatura) {
    Object.assign(this.faturaSelecionada, fatura);
  }

  onError(erro: Erro)  {
      const toast = this.notificationService
          .error(erro.codigo, erro.mensagem,  {
              timeOut: 3000,
              showProgressBar: true,
              pauseOnHover: true,
              clickToClose: false,
              clickIconToClose: true,
          });
  }

  findAll() {
      let array: Observable<Array<Fatura>> =  this.httpClient.get<Array<Fatura>>(this.url)
      if (array != null) {
        this.faturas = new Array<Fatura>();
        array.forEach(k => this.faturas = k.sort((a, b) => a.idFatura.valueOf() - b.idFatura.valueOf()))
      }
                     
  }

  onSucessMensage(title: string, mensagem: string) {
      const toast = this.notificationService
                        .success(title, mensagem, {
                              timeOut: 3000,
                              showProgressBar: true,
                              pauseOnHover: true,
                              clickToClose: false,
                              clickIconToClose: true,
                        });
  }
}
