import {Erro} from "./erro";

export class Fatura extends Erro {
    public idFatura: Number;
    public idUsuario: Number;
    public nomeEmpresa: string;
    public valor: Number;
    public dataVencimento: Date;
    public pago: boolean;
}