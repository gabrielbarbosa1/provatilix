import { Component } from '@angular/core';

@Component({
  selector: 'ngx-ztilixpages',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class ZtilixpagesComponent {
}
