package visao

import "net/http"

type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

type Routes []Route

var routes = Routes{
	Route{
		"PostFaturas",
		"POST",
		"/faturas",
		PostFatura,
	},
	Route{
		"PutFaturas",
		"PUT",
		"/faturas",
		PutFatura,
	},
	Route{
		"DeleteFaturas",
		"DELETE",
		"/faturas/{id}",
		DeleteFatura,
	},
	Route{
		"GetFaturas",
		"GET",
		"/faturas",
		GetFaturas,
	},
}
