package visao

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"provatilix/model"
)

type RespostaPadraoSimples struct {
	Mensagem string `json:"mensagem"`
}

func RespostaJson(w http.ResponseWriter, code int,  entidade interface{}) {
	response, _ := json.Marshal(entidade)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}

func ReadBody(r *http.Request) ([]byte) {
	body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576))
	if err != nil {
		panic(err)
	}
	if err := r.Body.Close(); err != nil {
		panic(err)
	}
	return body
}

func ParseJson(w http.ResponseWriter, body []byte, entidade interface{}) (*model.Erro) {
	if err := json.Unmarshal(body, entidade); err != nil {
		var erro model.Erro = model.Erro{"RESPOSTA_10","ERRO AO REALIZAR DESERIALIZAR JSON", err}
		fmt.Println(err)
		RespostaJson(w, http.StatusUnprocessableEntity, erro)
		return &erro
	}
	return nil
}