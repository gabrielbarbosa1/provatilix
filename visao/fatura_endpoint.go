package visao

import (
	"github.com/gorilla/mux"
	"net/http"
	"provatilix/aplicacao"
	"provatilix/model"
	"strconv"
)


func PostFatura(w http.ResponseWriter, r *http.Request) {
	var fatura model.Fatura
	var err *model.Erro
	if err  = ParseJson(w, ReadBody(r), &fatura); err == nil {
		if err, fatura = aplicacao.CadastrarFatura(fatura); err != nil {
			RespostaJson(w, http.StatusBadRequest, err)
			return
		}
		RespostaJson(w, http.StatusOK, fatura)
	}
}

func PutFatura(w http.ResponseWriter, r *http.Request) {
	var fatura model.Fatura
	var err *model.Erro
	if err  = ParseJson(w, ReadBody(r), &fatura); err == nil {
		if err, fatura = aplicacao.AtualizarFatura(fatura); err != nil {
			RespostaJson(w, http.StatusBadRequest, err)
			return
		}
		RespostaJson(w, http.StatusOK, fatura)
	}
}

func DeleteFatura(w http.ResponseWriter, r *http.Request) {
	var fatura model.Fatura
	var err *model.Erro
	vars := mux.Vars(r)
	id := vars["id"]
	if idFatura, errAtoi := strconv.Atoi(id); errAtoi == nil {
		if err = aplicacao.ExcluirFatura(idFatura); err != nil {
			RespostaJson(w, http.StatusBadRequest, err)
			return
		}
		RespostaJson(w, http.StatusOK, fatura)
	}
}

func GetFaturas(w http.ResponseWriter, r *http.Request) {
	var faturas []model.Fatura
	var err *model.Erro
	if err, faturas = aplicacao.ConsultarFatura(); err != nil {
		RespostaJson(w, http.StatusBadRequest, err)
		return
	}
	RespostaJson(w, http.StatusOK, faturas)
}


