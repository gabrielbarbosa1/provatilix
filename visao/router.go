package visao

import (
	"github.com/gorilla/mux"
	"net/http"
)

func NewRouter() *mux.Router {

	router := mux.NewRouter().StrictSlash(true)
	for _, route := range routes {

		//HANDLER DE LOG
		var logHandler http.Handler
		logHandler = route.HandlerFunc
		logHandler = Logger(logHandler, route.Name)


		if route.Name == "Index" ||
			route.Name == "PostLoginLogar" {
			router.
				Methods(route.Method).
				Path(route.Pattern).
				Name(route.Name).
				Handler(logHandler)
			continue
		}

		var authHandler http.Handler
		authHandler = route.HandlerFunc
		// authHandler = basicAuth(authHandler, route.Name)

		router.
			Methods(route.Method).
			Path(route.Pattern).
			Name(route.Name).
			Handler(logHandler).
			Handler(authHandler)
	}
	return router
}

