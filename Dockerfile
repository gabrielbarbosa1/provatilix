FROM  golang:1.11

ENV TZ=America/Sao_Paulo
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

WORKDIR /go/src

RUN go get github.com/lib/pq
RUN go get -u github.com/gorilla/mux
RUN go get -u github.com/gorilla/handlers
RUN go get github.com/rs/cors
RUN go get github.com/araddon/dateparse

ADD . /go/src/provatilix
RUN go install provatilix

EXPOSE 80
ENTRYPOINT /go/bin/provatilix