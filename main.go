package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"provatilix/aplicacao"
	"provatilix/visao"

	_ "github.com/lib/pq"
	"github.com/rs/cors"
)

func main() {

	if err := aplicacao.Init(); err != nil {
		fmt.Println("FALHA AO INICIALIZAR APP")
		os.Exit(1)
	}

	router := visao.NewRouter()
	stripPrefix := http.StripPrefix("/", http.FileServer(http.Dir("./../../go/src/provatilix/static")))
	router.PathPrefix("/").Handler(stripPrefix)
	http.Handle("/", router)
	log.Fatal(http.ListenAndServe(":80", cors.AllowAll().Handler(router)))
}
