package model

import (
	"github.com/gorilla/mux"
	"net/http"
	"strconv"
)

type Pagina struct {
	NumPagina     int         `json:"numPagina"`
	QtdPorPagina  int         `json:"qtdPagina"`
	TotalRegistro int         `json:"totalRegistro"`
	TotalPagina   int         `json:"totalPagina"`
	Conteudo      interface{} `json:"conteudo"`
}

func CalcQtdPaginas(totalRegistro int, qtdPorPagina int) (totalPagina int) {
	totalPagina = (totalRegistro + (qtdPorPagina - (totalRegistro % qtdPorPagina))) / qtdPorPagina
	return totalPagina
}

func CriaPagina(r *http.Request) (errDominio *Erro, paginaSolicitada Pagina) {
	var err error
	vars := mux.Vars(r)
	if paginaSolicitada.NumPagina, err = strconv.Atoi(vars["numPagina"]); err != nil {
		errDominio = &Erro{"CONTROLLER_ANUNCIO_10", "Erro ao havaliar parâmetro request, numPagina", err}
	}
	if paginaSolicitada.QtdPorPagina, err = strconv.Atoi(vars["qtdPorPagina"]); err != nil {
		errDominio = &Erro{"CONTROLLER_ANUNCIO_10", "Erro ao havaliar parâmetro request, qtdPorPagina", err}
	}
	return errDominio, paginaSolicitada
}