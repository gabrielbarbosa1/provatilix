package model

import (
	"github.com/araddon/dateparse"
	"provatilix/infraestrutura"
)

type Fatura struct {
	IdFatura int `json:"idFatura"`
	IdUsuario int `json:"idUsuario"`
	NomeEmpresa string `json:"nomeEmpresa"`
	Valor float32 `json:"valor"`
	DataVencimento string `json:"dataVencimento"`
	Pago bool `json:"pago"`
}

const TAM_NOME_EMPRESA = 100

func NewFatura(fat *Fatura) (erro *Erro, fatura *Fatura) {
	if fat.IdUsuario < 0 {
		return &Erro{"Fatura10", "Id do usúario inválido", nil}, nil
	}
	if fat.NomeEmpresa == "" || len(fat.NomeEmpresa) > TAM_NOME_EMPRESA {
		return &Erro{"Fatura20", "Nome empresa vázio ou com tamanho superior a 100 caracteres.", nil }, nil
	}
	if fat.Valor <= 0 {
		return &Erro{"Fatura30", "Valor da fatura com valor zero o negativo.", nil}, nil
	}
	if  _, errDate := dateparse.ParseIn(fat.DataVencimento, infraestrutura.TimeZonePadrao()); errDate != nil {
		return &Erro{"Fatura30", "Data de vencimento com formato invalido.", nil}, nil
	}
	return nil, fat
}
