package model

type Erro struct {
	Codigo   string `json:"codigo" example:"Login10"`
	Mensagem string `json:"mensagem" example:"Login vázio ou maior que 20 caracteres, inválido."`
	Err      error  `json:"err"`
}
